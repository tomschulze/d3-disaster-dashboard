const express = require("express");
const webpack = require("webpack");
const webpackDevMiddleware = require("webpack-dev-middleware");
const path = require("path");
const { readFile } = require("fs");

const app = express();
const port = 3000;
const config = require("../webpack.config.js");
config.mode = "development";
const compiler = webpack(config);

app.use(
  webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
  }),
);

app.get("/data", (req, res) => {
  readFile(path.join(__dirname, "emdat_data.json"), (err, json) => {
    if (err) throw err;
    const data = JSON.parse(json);
    res.json(data["Storm"]);
  });
});

app.listen(port, () => {
  console.log(`Development server listening on http://0.0.0.0:${port}`);
});
