import { json } from "d3-fetch";
import { DisasterDashboard } from "./disaster-dashboard";
import "../scss/main.scss";

// data: {1990: {'DEU': 233, 'ARG': 222, ...}, 1991: ... }
json("/data")
  .then((data) => {
    const options = {
      elementId: "disaster-dashboard",
    };

    const dashboard = new DisasterDashboard(data, options);
    dashboard.init();
  })
  .catch((error) => {
    throw error;
  });
