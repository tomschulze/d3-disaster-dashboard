import { create, select } from "d3-selection";
import { max, min } from "d3-array";
import { BarChart } from "./BarChart";
import { Legend } from "./Legend";
import { Map } from "./Map";
import { colorScale } from "./utils";

class DisasterDashboard {
  constructor(data, { elementId }) {
    this.data = data;
    this.elementId = elementId;
    this.initYear = 1990;
    this.headlineText = "Number of deaths caused by storms in ";
    this.barChartHeight = 200;
    this.mapHeight = 425;
    this.dashboardWidth = 960;
    this.sliderId = "slider-year";
    this.selector = {
      slider: "#" + this.sliderId,
    };
    this.minYear = parseInt(min(Object.keys(this.data)));
    this.maxYear = parseInt(max(Object.keys(this.data)));

    const options = { colorScale };

    const barChartOptions = Object.assign({}, options, {
      elementId: "bars",
      dimensions: {
        width: this.dashboardWidth,
        height: this.barChartHeight,
      },
    });

    const mapOptions = Object.assign({}, options, {
      elementId: "map",
      dimensions: {
        height: this.mapHeight,
        width: this.dashboardWidth,
      },
    });

    const legendOptions = Object.assign({}, options, {
      elementId: "legend",
      yPos: this.mapHeight,
      mapElementId: mapOptions.elementId,
    });

    this.legend = new Legend(legendOptions);
    this.map = new Map(mapOptions);
    this.barChart = new BarChart(barChartOptions);
    this.header = this.#createHeader();
    this.slider = this.#createSlider();
  }

  #getData(year) {
    return this.data[year];
  }

  #createSlider() {
    const slider = create("div").attr("id", "slider-container");

    slider
      .append("input")
      .attr("id", this.sliderId)
      .attr("type", "range")
      .attr("min", this.minYear)
      .attr("max", this.maxYear)
      .attr("value", this.init_year)
      .on("change", (event) => this.#updateAll(event.target.value));

    return slider.node();
  }

  #createHeader() {
    const h = create("div").attr("id", "header");

    h.append("h3")
      .attr("id", "headline")
      .text(this.headlineText + this.initYear);

    h.append("p").text(
      "Use the slider or the left and right keys to change the year. Hover over legend labels to highlight countries.",
    );
    h.append("p")
      .text("Data source: ")
      .append("a")
      .attr("href", "https://emdat.be")
      .text("EM-DAT 2017");

    return h.node();
  }

  #updateAll(year) {
    const data = this.#getData(year);

    select("#headline").text(this.headlineText + year);
    this.map.update(data);
    this.barChart.update(data);
    this.legend.update(data);
  }

  #arrowKeysEventHandler(event) {
    const changeYear = (year) => {
      select(this.selector.slider).node().value = year;
      select(this.selector.slider).dispatch("change");
    };

    const currentYear = () =>
      parseInt(select(this.selector.slider).node().value);

    if (event.key == "ArrowLeft") {
      if (currentYear() === this.minYear) {
        changeYear(this.maxYear);
      } else {
        changeYear(currentYear() - 1);
      }
    } else if (event.key == "ArrowRight") {
      if (currentYear() === this.maxYear) {
        changeYear(this.minYear);
      } else {
        changeYear(currentYear() + 1);
      }
    }
  }

  init() {
    document.addEventListener("keyup", this.#arrowKeysEventHandler.bind(this));

    const id = "#" + this.elementId;
    const initData = this.#getData(this.initYear);

    select(id).insert(() => this.header);
    select(id).insert(() => this.slider);
    select(id)
      .insert(() => this.map.getNode())
      .insert(() => this.legend.getNode());
    select(id).insert(() => this.barChart.getNode());

    this.map.update(initData);
    this.barChart.update(initData);
    this.legend.update(initData);
  }
}

export { DisasterDashboard };
