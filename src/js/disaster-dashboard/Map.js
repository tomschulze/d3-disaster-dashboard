import { selectAll } from "d3-selection";
import "d3-transition";
import { geoPath } from "d3-geo";
import { geoRobinson } from "d3-geo-projection";
import { create } from "d3-selection";
import { rgb } from "d3-color";
import { feature } from "topojson-client";

import topoJson from "./world.topo.json";

class Map {
  constructor({ elementId, colorScale, dimensions }) {
    this.elementId = elementId;
    this.colorScale = colorScale;
    this.dimensions = dimensions;

    this.data = null;
    this.selector = {
      paths: "svg#map path",
      titles: "svg#map path title",
      pathsOtherThanColorKey: (colorKey) => {
        return "svg#map path:not([fill = '" + rgb(colorKey) + "'])";
      },
    };

    this.colorInit = rgb("#fff");
    this.colorUndefined = rgb("#d4d4d4");
    this.topoJson = topoJson;
    this.path = geoPath(geoRobinson());
    this.node = this.#createNode();
  }

  #createNode() {
    const map = create("svg:svg")
      .attr("id", this.elementId)
      .attr("height", this.dimensions.height)
      .attr("width", this.dimensions.width)
      .on("map-highlight", (event) => {
        const colorKey = event.detail.colorKey;

        selectAll(this.selector.paths)
          .interrupt() //cancels ongoing transitions (e.g., for quick mouseovers)
          .call(this.#fillMap, this);

        selectAll(this.selector.pathsOtherThanColorKey(colorKey)).attr(
          "fill",
          this.colorUndefined,
        );
      })
      .on("map-refill", () => {
        selectAll(this.selector.paths)
          .transition()
          .delay(100)
          .call(this.#fillMap, this);
      });

    map
      .append("g")
      .attr("class", "countries")
      .selectAll("path")
      .data(feature(this.topoJson, this.topoJson.objects.world).features)
      .enter()
      .append("path")
      .attr("d", this.path)
      .attr("fill", this.colorInit)
      .attr("id", function (d) {
        return d.id;
      })
      .append("title");

    return map.node();
  }

  #fillMap(selection, that) {
    const data = that.data;
    const colorUndefined = that.colorUndefined;
    const color = that.colorScale(data);

    selection.attr("fill", function (d) {
      if (typeof data[d.id] === "undefined") {
        return colorUndefined;
      }

      return rgb(color(data[d.id]));
    });
  }

  #setPathTitle(selection, that) {
    const data = that.data;

    selection.text((d) => {
      const value = typeof data[d.id] === "undefined" ? "N/A" : data[d.id];
      return "" + d.id + ", " + value;
    });
  }

  getNode() {
    return this.node;
  }

  update(data) {
    this.data = data;

    selectAll(this.selector.paths)
      .transition()
      .delay(100)
      .call(this.#fillMap, this);

    selectAll(this.selector.titles).call(this.#setPathTitle, this);
  }
}

export { Map };
