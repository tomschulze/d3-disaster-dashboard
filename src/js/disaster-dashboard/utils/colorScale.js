import { scaleQuantile } from "d3-scale";
import { schemeBlues } from "d3-scale-chromatic";

const colorScale = (data) => {
  return scaleQuantile().domain(Object.values(data)).range(schemeBlues[5]);
};

export { colorScale };
