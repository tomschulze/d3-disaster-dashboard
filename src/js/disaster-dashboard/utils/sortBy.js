// sorts an array of records by key
// assumes that sortkey has no duplicates
const sortBy = (array, key) => {
  const sortedKeys = array.map((el) => el[key]).sort();

  const newArray = [];
  for (let k of sortedKeys) {
    for (let i in array) {
      if (array[i][key] === k) {
        newArray.push(array[i]);
        continue;
      }
    }
  }

  return newArray;
};

export { sortBy };
