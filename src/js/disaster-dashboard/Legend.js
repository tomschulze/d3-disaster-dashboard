import { create, select } from "d3-selection";
import { rgb } from "d3-color";
import { format } from "d3-format";

class Legend {
  constructor({ elementId, colorScale, yPos, mapElementId }) {
    this.elementId = elementId;
    this.colorScale = colorScale;
    this.yPos = yPos;
    this.mapElementId = mapElementId;

    this.data = null;
    this.color = null;
    this.selector = {
      legend: "svg#map g#" + this.elementId.Legend,
      items: "svg#map g#" + this.elementId + "-items",
    };

    this.node = this.#createNode();
  }

  #createNode() {
    const legend = create("svg:g")
      .attr("id", this.elementId)
      .attr("class", "legend");

    legend
      .append("g")
      .attr("id", this.elementId + "-title")
      .attr("class", "legend-title")
      .append("text")
      .text("Legend (quintile ranges)")
      .attr("x", 30)
      .attr("y", this.yPos - 140);

    legend
      .append("g")
      .attr("id", this.elementId + "-items")
      .attr("class", "legend-items");

    return legend.node();
  }

  #calculateLegendLabels(quantiles) {
    const lastIndex = quantiles.length - 1;
    const formatter = format("d");
    const formattedQuantiles = quantiles.map((el) => formatter(el));

    const labels = formattedQuantiles.map((el, idx) => {
      if (idx == 0) {
        return "< " + formattedQuantiles[0];
      }

      return formattedQuantiles[idx - 1] + " - " + el;
    });

    const lastLabel = "> " + formattedQuantiles[lastIndex];
    labels.push(lastLabel);
    return labels;
  }

  getNode() {
    return this.node;
  }

  update(data) {
    this.data = data;
    this.color = this.colorScale(this.data);

    select(this.selector.items)
      .selectAll("rect")
      .data(this.color.range())
      .join("rect")
      .attr("width", "20")
      .attr("height", "20")
      .attr("y", (d, i) => this.yPos - 29 - 25 * i)
      .attr("x", 30)
      .attr("fill", (d) => rgb(d))
      .on("mouseover", (event) => {
        const d = event.target.__data__;
        const options = { detail: { colorKey: d } };
        select("svg#" + this.mapElementId).dispatch("map-highlight", options);
      })
      .on("mouseout", (event) => {
        select("svg#" + this.mapElementId).dispatch("map-refill");
      });

    const legendItems = this.#calculateLegendLabels(this.color.quantiles());

    select(this.selector.items)
      .selectAll("text")
      .data(legendItems)
      .join("text")
      .attr("y", (d, i) => this.yPos - 14 - 25 * i)
      .attr("x", 60)
      .text((d) => d);
  }
}

export { Legend };
