import { create, select } from "d3-selection";
import { scaleLinear, scaleBand } from "d3-scale";
import { max } from "d3-array";
import { axisBottom } from "d3-axis";
import { format } from "d3-format";

import { sortBy } from "./utils";

class BarChart {
  constructor({ elementId, colorScale, dimensions }) {
    this.elementId = elementId;
    this.colorScale = colorScale;
    this.dimensions = dimensions;

    this.data = null;
    this.color = null;
    this.selector = {
      chart: "svg#" + this.elementId,
      axes: "svg#" + this.elementId + " g.axis",
      bars: "svg#" + this.elementId + " g.bars",
    };

    this.margin = { top: 50, right: 10, bottom: 50, left: 30 };
    this.innerWidth =
      this.dimensions.width - this.margin.left - this.margin.right;
    this.innerHeight =
      this.dimensions.height - this.margin.top - this.margin.bottom;
    this.x = scaleBand().rangeRound([0, this.innerWidth]).padding(0.05);
    this.y = scaleLinear().range([this.innerHeight, 0]);
    this.node = this.#createNode();
  }

  #createNode() {
    const bars = create("svg:svg")
      .attr("id", this.elementId)
      .attr("width", this.dimensions.width)
      .attr("height", this.dimensions.height);

    bars
      .append("g")
      .attr("class", "bars")
      .attr(
        "transform",
        "translate(" + this.margin.left + ", " + this.margin.top + ")",
      );

    return bars.node();
  }

  // { BRA: 234, DEU: 2 } => [{id: 'BRA', value: 234 }, ...]
  #transformData(data) {
    let transformed = [];
    for (let key of Object.keys(data)) {
      transformed.push({ id: key, value: data[key] });
    }

    transformed = sortBy(transformed, "id");
    return transformed;
  }

  getNode() {
    return this.node;
  }

  update(data) {
    this.color = this.colorScale(data);
    this.data = this.#transformData(data);
    this.#updateAxes();
    this.#updateBars();
    this.#updateAnnotations();
  }

  #updateAxes() {
    this.x.domain(this.data.map((d) => d.id));
    this.y.domain([0, max(this.data, (d) => d.value)]);

    select(this.selector.axes).remove();
    select(this.selector.chart)
      .append("g")
      .attr("class", "axis axis--x")
      .attr(
        "transform",
        "translate(" + 30 + "," + (this.innerHeight + this.margin.top) + ")",
      )
      .call(axisBottom(this.x))
      .selectAll("text")
      .style("text-anchor", "end")
      .attr("dx", "-.8em")
      .attr("dy", ".15em")
      .attr("transform", "rotate(-65)");
  }

  #updateBars() {
    select(this.selector.bars)
      .selectAll("rect")
      .data(this.data)
      .join("rect")
      .attr("fill", (d) => this.color(d.value))
      .attr("x", (d) => this.x(d.id))
      .attr("width", this.x.bandwidth())
      .attr("y", (d) => this.y(d.value))
      .attr("height", (d) => this.innerHeight - this.y(d.value));
  }

  #updateAnnotations() {
    select(this.selector.bars)
      .selectAll("text")
      .data(this.data)
      .join("text")
      .text((d) => format(",")(d.value))
      .attr("class", "bar-label")
      .attr("x", (d) => this.x(d.id) + this.x.bandwidth() / 2)
      .attr("y", (d) => this.y(d.value) - 5);
  }
}

export { BarChart };
