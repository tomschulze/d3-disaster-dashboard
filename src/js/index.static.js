import { DisasterDashboard } from './disaster-dashboard'
import '../scss/main.scss'
import json from '../../dev/emdat_data.json'

const { Storm: data } = json

// data: {1990: {'DEU': 233, 'ARG': 222, ...}, 1991: ... }
const options = {
  elementId: 'disaster-dashboard',
}

const dashboard = new DisasterDashboard(data, options)
dashboard.init()
