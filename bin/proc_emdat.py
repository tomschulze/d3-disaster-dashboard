#!/usr/bin/python

import pandas as pd
import json

PATH_IN = "data/emdat_data.csv"
PATH_OUT = "dist/emdat_data.json"

cols = ['year', 'iso', 'ctryname', 'disastertype', 'occurence',
        'deaths', 'injuries', 'affected', 'homeless',
        'affected_total', 'damage_total']

df = pd.read_csv(PATH_IN, sep=',', names=cols, header=0)
print "shape before nan removal: {0}".format(df.shape)

df = df.dropna(axis=0, subset=['deaths'])
print "shape after nan removal: {0}".format(df.shape)

df = df.groupby('disastertype')

dict_json = { "Storm": {} }
dfg = df.get_group("Storm").groupby('year')

for k,v in dfg:
    if k in range(1990,2017):
        v.index = v['iso']
        dict_json['Storm'][k] = v['deaths'].to_dict()
        
with open(PATH_OUT, 'w') as f:
    f.write(json.dumps(dict_json))

print "json saved to {0}".format(PATH_OUT)
