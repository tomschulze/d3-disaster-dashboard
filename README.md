## Disaster dashboard

A dashboard based on d3.js. It shows the number of deaths caused by storms from 1990 to 2016. The dashboard consists of a world map, a bar chart and a legend. A slider lets the user scroll through the years, displaying a cross section of the data.

![Disaster Dashboard](./disaster-dashboard.png)

Data source: [EM-DAT][], Center for Research on the Epidemiology of Disasters (CRED), downloaded in 2017

[EM-DAT]: https://www.emdat.be/
[naturalearthdata.com]: https://www.naturalearthdata.com/
[mapshaper.org]: https://www.mapshaper.org
[demo]: https://tomschulze.gitlab.io/d3-disaster-dashboard

The raw data in `data/emdat_data.csv` was processed using `bin/proc_emdat.py`. The map file `js/src/wordl.topo.json` was obtained from [naturalearthdata.com][] and edited using [mapshaper.org][]

## Try it

With the [demo][] on Gitlab Pages.

With the webpack dev server:

- `npm install && npm run dev`
- access http://127.0.0.1:3000

## Develop

Run `npm run dev` to spin up a custom `webpack-dev-server`, see `dev/server.js`. Reload the page to trigger the bundler after your changes.

## Build

The command `npm run build` creates a production build in the `public/` folder containing `css/bundle.css`, `js/bundle.js` and a sample `index.html`.

The command `npm run build-static` creates a static production build meaning the json data is included in the js code.

## Todos

- maybe implement a way to scale svgs for responsiveness
- when hovering the legend it would be nice to highlight the respective countries in the bar chart as well
