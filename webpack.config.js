const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const InjectBodyPlugin = require('inject-body-webpack-plugin').default

const htmlWebpackPlugin = new HtmlWebpackPlugin({
  title: 'D3 Disaster Dashboard Demo',
})

const injectBodyPlugin = new InjectBodyPlugin({
  content: '<div id="disaster-dashboard"></div>',
})

const miniCssExtractPlugin = new MiniCssExtractPlugin({
  filename: 'css/bundle.css',
})

const entryPoint = process.env.USE_STATIC_BUILD ? 'index.static.js' : 'index.js'

module.exports = {
  entry: path.resolve(__dirname, 'src', 'js', entryPoint),
  devServer: {
    static: './dist/',
  },
  plugins: [miniCssExtractPlugin, htmlWebpackPlugin, injectBodyPlugin],
  module: {
    rules: [
      {
        test: /\.scss$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
      },
      {
        test: /\.json$/,
        type: 'json',
      },
    ],
  },
  output: {
    filename: 'js/bundle.js',
    path: path.resolve(__dirname, 'public'),
    clean: true,
    publicPath: '',
  },
}
